class BakeCookiesJob < ApplicationJob
  queue_as :default

  def perform(cookie)
    cookie.update(is_cooked: true)
  end
end
