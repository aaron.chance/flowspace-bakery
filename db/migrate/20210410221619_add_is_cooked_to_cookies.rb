class AddIsCookedToCookies < ActiveRecord::Migration[5.1]
  def change
    add_column :cookies, :is_cooked, :boolean, default:false
  end
end
